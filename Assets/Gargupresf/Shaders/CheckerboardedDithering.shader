﻿Shader "Image Effects/Checkerboarded Dithering"
{
	Properties
	{
		_MainTex ("Texture", 2D) = "white" {}
		_Noise ("Noise", 2D) = "white" {}
		_TexelSizeX ("Texel size X", Range(0,1)) = 1
		_TexelSizeY ("Texel size Y", Range(0,1)) = 1
		_ScreenSizeX ("Screen size X", Float) = 1366
		_ScreenSizeY ("Screen size Y", Float) = 768
		_ScaleDiv ("Scale division", Float) = 768
		_Sharpness ("Sharpness", Range(0,16)) = 4
		_Dither ("Use Dithering?", Range(0,1)) = 1
	}
	SubShader
	{
		Cull Off ZWrite Off

		Pass
		{
			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag

			#include "UnityCG.cginc"

			struct appdata
			{
				float4 vertex : POSITION;
				float2 uv : TEXCOORD0;
			};

			struct v2f
			{
				float2 uv : TEXCOORD0;
				float4 vertex : SV_POSITION;
			};

			v2f vert (appdata v)
			{
				v2f o;
				o.vertex = UnityObjectToClipPos(v.vertex);
				o.uv = v.uv;
			   return o;
			}

			uniform sampler2D _MainTex;
			sampler2D _Noise;
			half _TexelSizeX;
			half _TexelSizeY;
			half _ScreenSizeX;
			half _ScreenSizeY;
			half _ScaleDiv;
			half _Sharpness;
			half _Dither;

			float4 dither (float2 uv)
			{
				float4 c1;
				float4 c2;
				float4 c3;
				float4 c4;
				
				c1 = tex2D(_MainTex, uv + float2(_TexelSizeX, _TexelSizeY));
				c2 = tex2D(_MainTex, uv + float2(-_TexelSizeX, -_TexelSizeY));
				c3 = tex2D(_MainTex, uv + float2(-_TexelSizeX, _TexelSizeY));
				c4 = tex2D(_MainTex, uv + float2(_TexelSizeX, -_TexelSizeY));
				
				return lerp(lerp(c1, c2, tex2D(_Noise, uv + _Time.x).r), lerp(c3, c4, tex2D(_Noise, uv - _Time.x * 2).r), tex2D(_Noise, uv * _Time.x * 4).r);
			}
			fixed4 dothething (float2 uv)
			{
				float moduloX = (uv.x * _ScreenSizeX) % _ScaleDiv;
				float moduloY = (uv.y * _ScreenSizeY) % _ScaleDiv;
				if ((moduloY < _ScaleDiv * .5 && moduloX < _ScaleDiv * .5) || (moduloY > _ScaleDiv * .5 && moduloX > _ScaleDiv * .5))
				//	return fixed4(0,0,0,1); Uncomment this to visualize the grid
					return tex2D(_MainTex, uv);

				return dither(uv);
			}
			fixed4 frag (v2f i) : SV_Target
			{
				float2 uv = i.uv - half2(_TexelSizeX, _TexelSizeY) * .5;
				
				if (_Dither > .5)
					return lerp(dothething(uv), dothething(uv + half2(_TexelSizeX, _TexelSizeY)), _Sharpness);
				else
					return lerp(tex2D(_MainTex, uv), tex2D(_MainTex, uv + half2(_TexelSizeX, _TexelSizeY)), _Sharpness);
			}
			ENDCG
		}
	}
}
