﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CheckerboardedDithering : MonoBehaviour
{
	public float scale = .5f;
	public Camera thisCamera;
	public Material imageEffectMaterial;
	public UnityEngine.UI.RawImage UICanvasRawImage;
	RenderTexture upscaled;
	void OnRenderImage(Texture src, RenderTexture dst)
	{
		if (
			!upscaled ||
			upscaled.height != Screen.height ||
			upscaled.width != Screen.width ||
			thisCamera.rect.xMax != scale
		)
		{
			if (upscaled)
			{
				upscaled.Release();
				DestroyImmediate(upscaled);
			}
			upscaled = new RenderTexture(Screen.width, Screen.height, 0);
			
			UICanvasRawImage.enabled = true;
			UICanvasRawImage.texture = upscaled;
			
			thisCamera.rect = new Rect(0,0,scale,scale); //Render scale
			float sclDiv = 1.0f / scale; //One divided by the render scale
			imageEffectMaterial.SetFloat("_ScaleDiv", sclDiv);
			imageEffectMaterial.SetFloat("_TexelSizeX", scale * .5f / (float)Screen.width); //Half of sclDiv divided by screen dimensions
			imageEffectMaterial.SetFloat("_TexelSizeY", scale * .5f / (float)Screen.height); //Half of sclDiv divided by screen dimensions
			imageEffectMaterial.SetFloat("_ScreenSizeX", Screen.width);
			imageEffectMaterial.SetFloat("_ScreenSizeY", Screen.height);
		}
		
		upscaled.DiscardContents(true, true);
		Graphics.Blit(src, upscaled, imageEffectMaterial);
	}
	void OnDisable()
	{
		OnDestroy();
	}
	void OnDestroy()
	{
		if (upscaled)
		{
			upscaled.Release();
			DestroyImmediate(upscaled);
		}
		
		UICanvasRawImage.enabled = false;
		UICanvasRawImage.texture = null;
		
		thisCamera.rect = new Rect(0,0,1,1);
	}
}
